#include <kounter.hpp>

Kounter::Kounter(int k) : _n(0), _k(k) {
}

int Kounter::getN() const {
  return _n;
}

void Kounter::countK() {
  _n += _k;
}

