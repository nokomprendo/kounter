#ifndef KOUNTER_HPP
#define KOUNTER_HPP

class Kounter {
  private:
    int _n;
    int _k;
  public:
    Kounter(int k);
    int getN() const;
    void countK();
};

#endif // KOUNTER_HPP

