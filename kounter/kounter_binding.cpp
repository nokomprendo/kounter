#include <kounter.hpp>

#include <boost/python.hpp>

using namespace boost::python;

BOOST_PYTHON_MODULE( kounter ) {
  class_<Kounter>("Kounter", init<int>())
    .def("getN", &Kounter::getN)
    .def("countK", &Kounter::countK)
    ;
}

