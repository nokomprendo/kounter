from setuptools import setup, Extension

kounter_module = Extension('kounter',
    include_dirs = ['kounter'],
    sources = ['kounter/kounter.cpp', 'kounter/kounter_binding.cpp'],
    libraries = ['boost_python'])

setup(name = 'pykounter',
    version = '0.1',
    packages = ['pykounter'],
    ext_modules = [kounter_module])

