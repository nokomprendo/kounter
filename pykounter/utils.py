#!/usr/bin/env python

import kounter

def create_kounter42():
    return kounter.Kounter(42)

def count_and_print(kntr):
    kntr.countK()
    print('pykounter:', kntr.getN())

if __name__ == '__main__':
    kntr = create_kounter42()
    count_and_print(kntr)
    count_and_print(kntr)

